# KBC storage writer

## Description

Writer for Keboola storage.

##### What it is used for?

Writing the csv files from local machine to KBC storage.

##### What the code does?

The code - especially main.py file does:

1. Load the config file, where are all necessary information for the extractor.
2. Connect to KBC by official python client and upload specified tables.

## Requirements

* python3.9 or higher
* KBC token with access to storage

## How to use it?

### Configuration file
See example `config.sample.json` you can find out more in json schema `config.schema.json`


### Local use

1) Create or modify the config.json file (see example `config.sample.json`):

2) Run the /scr/main.py
      ```sh
      python3 main.py
      ```

#### script parameters
 * --config \<path to config file\>
 * --input \<path to input directory>
 * --config-schema ../config.schema.json (path to json schema file)
