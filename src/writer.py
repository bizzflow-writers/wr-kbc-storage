import logging

from kbcstorage.client import Client

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class KBCStorageWriter:
    def __init__(self, host: str, token: str, ):
        self.host = host
        self.token = token
        self.client = Client(host, token)

    def _get_existing_table_ids(self):
        tables = self.client.tables.list()
        return [table["id"] for table in tables]

    def write_table(self, table_id, source_file, incremental=False, primary_key=None):
        if table_id in self._get_existing_table_ids():
            logger.info(f"Table {table_id} already exists uploading data, incremental={incremental}")
            self.client.tables.load(table_id, source_file, is_incremental=incremental)
        else:
            logger.info(f"Table {table_id} do not exists, creating and uploading data, primary_key={primary_key}")
            bucket_id, table_name = table_id.rsplit(".", 1)
            self.client.tables.create(bucket_id, table_name, source_file, primary_key=primary_key)
        logger.info(f"Table {table_id} was successfully uploaded")

