import os

from bizztreat_base.config import Config

from writer import KBCStorageWriter


def main():
    conf = Config()
    extractor = KBCStorageWriter(
        host=conf["host"],
        token=conf["token"],
    )
    for table_config in conf["tables"]:
        file_path = os.path.join(conf.input_folder, f"{table_config['input_table_name']}.csv")
        extractor.write_table(
            table_id=table_config["kbc_table_id"],
            source_file=file_path,
            incremental=table_config.get("incremental", False),
            primary_key=table_config.get("primary_key"),
        )


if __name__ == "__main__":
    main()
